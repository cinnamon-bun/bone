
alias ll="ls -la"
alias u="cd .."
alias uu="cd ../.."
alias pow="echo -------------------------------------------------------------------"

alias cdgo="cd /home/root/gocode/src/github.com/longears/pixelslinger"

alias off="~/off.sh"

PS1="BEAGLEBONE-B \\u@\\h:\\w\\$ "; export PS1

export PATH=$PATH:/usr/local/go/bin
export GOPATH=/home/root/gocode
export GOROOT=/usr/local/go

