" designed to match gedit's default colors

:colorscheme morning
:hi Normal guibg=#f0f0f0
:hi String guifg=#ff00ff guibg=#f0f0f0
:hi Function guifg=#002020 gui=bold

hi link pythonFunction    Function


hi Cursor    guifg=white guibg=blue
" hi Cursor    guifg=black guibg=#ffaf38
