version 6.0

set nocompatible
let s:cpo_save=&cpo
set cpo&vim
map! <xHome> <Home>
map! <xEnd> <End>
map! <S-xF4> <S-F4>
map! <S-xF3> <S-F3>
map! <S-xF2> <S-F2>
map! <S-xF1> <S-F1>
map! <xF4> <F4>
map! <xF3> <F3>
map! <xF2> <F2>
map! <xF1> <F1>

map <xHome> <Home>
map <xEnd> <End>
map <S-xF4> <S-F4>
map <S-xF3> <S-F3>
map <S-xF2> <S-F2>
map <S-xF1> <S-F1>
map <xF4> <F4>
map <xF3> <F3>
map <xF2> <F2>
map <xF1> <F1>
let &cpo=s:cpo_save
unlet s:cpo_save


" fix backspace key in insert mode
" imap <BS> <Left><Del>

" basic setup
set fileencodings=ucs-bom,utf-8,latin1
" set helplang=en
set termencoding=utf-8
set hidden

" interface
set cmdheight=2
set mouse=a
" set statusline=%F\ %m\ %r\ %h\ %w\ %4l,%3v\ (%p%%)
set ruler
set laststatus=2
set noequalalways
set number

" tab stuff
set autoindent
set shiftwidth=4
set tabstop=4
set softtabstop=4
set expandtab
filetype on
filetype indent on

" search
set hlsearch
set incsearch
set showmatch
map <F12> :set hlsearch!<CR>
map \\ :set hlsearch!<CR>
set ignorecase
set smartcase

" unknown purpose
set modelines=0

" execute python file being edited with Shift-E
" or to execute file directly, :!%
" map <S-e> :w<CR>:!/usr/bin/env python2 % <CR>

" syntax highlighting
syntax enable

" turn on highlights for indent levels in a python file not using tabs
map \t :match Search /\%(\_^\s*\)\@<=\%(\%1v\\|\%5v\\|\%9v\)\s/<CR>

" highlight line 80
" highlight OverLength ctermbg=red ctermfg=white guibg=#592929
map \8 :match Search /\%81v.\+/<CR>

" comment and un-comment for python code in normal or visual mode.
vnoremap <F9> :normal 0i# <CR>   " add "# " to front of line
vnoremap <F7> :normal 02x<CR>    " remove first two chars from line
nnoremap <F9> :normal 0i# <CR>
nnoremap <F7> :normal 02x<CR>

" better vim-command-line tab completion
set wildmenu

" tab completion mode
set wildmode=longest,list,full

" remove all blank lines
" :%s/^ *\n//g

" insert blank lines above/below
map <S-Enter> O<Esc>
map <CR> o<Esc>

" make backspace work in normal mode
" make backspace work across lines
" iunmap <BS>
set backspace=indent,eol,start

" make backspace delete stuff in normal mode
" this needs improvement to make it wrap across lines
nnoremap <BS> <Left><Del>

" type 12_ to insert a copy of line 12 right above cursor
nnoremap _ ggY``P

" unused remaps from when we put stuff on H and L
" nnoremap gH H
" nnoremap gM M  " for consistancy; this wasn't already used
" nnoremap gL L

" half-page scrolling
" need to find better keys for this
nnoremap J <C-D>
nnoremap K <C-U>

" keep this many lines visible above and below the cursor
set scrolloff=3

" use J and K for buffer switching, like switching tabs in Vimperator
" sacrificing: join lines together; man lookup word under cursor
" you can use gJ to get the old J functionality with no spaces,
" or :j [N] to get a join with spaces.
nnoremap <C-K> :bprevious<CR>
nnoremap <C-J> :bnext<CR>

" indent selected lines (builtin-repeat)
:vnoremap < <gv
:vnoremap > >gv

" disable scary commands that might
" be typed by accident
nnoremap ZZ <nop>
nnoremap ZQ <nop>


" set up unusual filetypes
au BufNewFile,BufRead *.zshot set filetype=sh
au BufNewFile,BufRead vimtips set filetype=vim
au BufNewFile,BufRead vimcheatsheet.txt set filetype=help
au BufNewFile,BufRead *.txt set filetype=help
au BufNewFile,BufRead *.yml.example set filetype=yaml
au BufNewFile,BufRead *.pde set filetype=java
au BufNewFile,BufRead *.json set filetype=python
au BufNewFile,BufRead *.moustache set filetype=html

au FileType crontab set nobackup nowritebackup

" 256 colors in console mode
set t_Co=256
"so ~/.vim/colorschemes/colorpack/railscasts2.vim
colorscheme railscasts2term

" switch cursor style in insert mode for consolve vim
let &t_SI = "\<Esc>]50;CursorShape=1\x7"
let &t_EI = "\<Esc>]50;CursorShape=0\x7"






" after doing this, use zR to open all the folds
" and use [c and ]c to jump to prev / next diff
" use do to obtain/pull changes into this window, and dp to push changes to other window 
" command DiffOrig vert new | set bt=nofile | r # | 0d_ | diffthis | wincmd p | diffthis




" Voom settings
let g:voom_tree_placement="left"
let g:voom_tree_width=50
let g:voom_return_key = '<C-Return>'

nnoremap <leader>o :Voom python<CR>

" golang
" Some Linux distributions set filetype in /etc/vimrc.
" Clear filetype flags before changing runtimepath to force Vim to reload them
filetype off
filetype plugin indent off
set runtimepath+=$GOROOT/misc/vim
filetype plugin indent on
syntax on


