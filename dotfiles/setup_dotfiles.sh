#!/bin/sh

cd ~

rm -f .profile
rm -f .bashrc
ln -s /home/root/bone/dotfiles/.profile .profile
ln -s /home/root/bone/dotfiles/.profile .bashrc

rm -f .gitconfig
ln -s /home/root/bone/dotfiles/.gitconfig .gitconfig

rm -f .vimrc
ln -s /home/root/bone/dotfiles/.vimrc .vimrc

rm -f .vim
ln -s /home/root/bone/dotfiles/.vim .vim

rm -f startup.sh
ln -s /home/root/bone/dotfiles/startup.sh startup.sh

rm -f off.sh
ln -s /home/root/bone/dotfiles/off.sh off.sh

rm -f ntp.sh
ln -s /home/root/bone/dotfiles/ntp.sh ntp.sh

