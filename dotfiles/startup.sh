#!/bin/sh

echo ==========================================
echo ========================================== STARTUP
echo ========================================== BLINKING ONBOARD LEDS...

echo 0 > /sys/class/leds/beaglebone\:green\:usr0/brightness
echo 0 > /sys/class/leds/beaglebone\:green\:usr1/brightness
echo 0 > /sys/class/leds/beaglebone\:green\:usr2/brightness
echo 0 > /sys/class/leds/beaglebone\:green\:usr3/brightness

sleep 0.1

echo 0 > /sys/class/leds/beaglebone\:green\:usr0/brightness
echo 0 > /sys/class/leds/beaglebone\:green\:usr1/brightness
echo 0 > /sys/class/leds/beaglebone\:green\:usr2/brightness
echo 1 > /sys/class/leds/beaglebone\:green\:usr3/brightness

sleep 0.1

echo 0 > /sys/class/leds/beaglebone\:green\:usr0/brightness
echo 0 > /sys/class/leds/beaglebone\:green\:usr1/brightness
echo 1 > /sys/class/leds/beaglebone\:green\:usr2/brightness
echo 1 > /sys/class/leds/beaglebone\:green\:usr3/brightness

sleep 0.1

echo 0 > /sys/class/leds/beaglebone\:green\:usr0/brightness
echo 1 > /sys/class/leds/beaglebone\:green\:usr1/brightness
echo 1 > /sys/class/leds/beaglebone\:green\:usr2/brightness
echo 0 > /sys/class/leds/beaglebone\:green\:usr3/brightness

sleep 0.1

echo 1 > /sys/class/leds/beaglebone\:green\:usr0/brightness
echo 1 > /sys/class/leds/beaglebone\:green\:usr1/brightness
echo 0 > /sys/class/leds/beaglebone\:green\:usr2/brightness
echo 0 > /sys/class/leds/beaglebone\:green\:usr3/brightness

sleep 0.1

echo 0 > /sys/class/leds/beaglebone\:green\:usr0/brightness
echo 1 > /sys/class/leds/beaglebone\:green\:usr1/brightness
echo 1 > /sys/class/leds/beaglebone\:green\:usr2/brightness
echo 0 > /sys/class/leds/beaglebone\:green\:usr3/brightness

sleep 0.1

echo 0 > /sys/class/leds/beaglebone\:green\:usr0/brightness
echo 0 > /sys/class/leds/beaglebone\:green\:usr1/brightness
echo 1 > /sys/class/leds/beaglebone\:green\:usr2/brightness
echo 1 > /sys/class/leds/beaglebone\:green\:usr3/brightness

sleep 0.1

echo 0 > /sys/class/leds/beaglebone\:green\:usr0/brightness
echo 0 > /sys/class/leds/beaglebone\:green\:usr1/brightness
echo 0 > /sys/class/leds/beaglebone\:green\:usr2/brightness
echo 1 > /sys/class/leds/beaglebone\:green\:usr3/brightness

sleep 0.1

echo 0 > /sys/class/leds/beaglebone\:green\:usr0/brightness
echo 0 > /sys/class/leds/beaglebone\:green\:usr1/brightness
echo 0 > /sys/class/leds/beaglebone\:green\:usr2/brightness
echo 0 > /sys/class/leds/beaglebone\:green\:usr3/brightness

sleep 0.1

echo ========================================== ENABLING SPI...
echo spi0 > /sys/devices/bone_capemgr.8/slots

sleep 0.05

echo ========================================== STARTING C SERVER...

echo 0 > /sys/class/leds/beaglebone\:green\:usr0/brightness
echo 0 > /sys/class/leds/beaglebone\:green\:usr1/brightness
echo 0 > /sys/class/leds/beaglebone\:green\:usr2/brightness
echo 1 > /sys/class/leds/beaglebone\:green\:usr3/brightness

/home/root/openpixelcontrol/bin/lpd8806_server &

sleep 1

echo ========================================== SENDING PIXELS TO INIT SPI...

echo 0 > /sys/class/leds/beaglebone\:green\:usr0/brightness
echo 0 > /sys/class/leds/beaglebone\:green\:usr1/brightness
echo 1 > /sys/class/leds/beaglebone\:green\:usr2/brightness
echo 0 > /sys/class/leds/beaglebone\:green\:usr3/brightness

cd /home/root/gocode/src/github.com/longears/pixelslinger
./pixelslinger --layout layouts/metal_tower_final.json --source raver-plaid --dest spi --once

echo 0 > /sys/class/leds/beaglebone\:green\:usr0/brightness

sleep 1

echo ========================================== KILLING C SERVER...

echo 0 > /sys/class/leds/beaglebone\:green\:usr0/brightness
echo 1 > /sys/class/leds/beaglebone\:green\:usr1/brightness
echo 0 > /sys/class/leds/beaglebone\:green\:usr2/brightness
echo 0 > /sys/class/leds/beaglebone\:green\:usr3/brightness

killall lpd8806_server

sleep 1

echo ========================================== STARTING MAIN GO PROGRAM...
echo 0 > /sys/class/leds/beaglebone\:green\:usr0/brightness
echo 1 > /sys/class/leds/beaglebone\:green\:usr1/brightness
echo 1 > /sys/class/leds/beaglebone\:green\:usr2/brightness
echo 1 > /sys/class/leds/beaglebone\:green\:usr3/brightness

cd /home/root/gocode/src/github.com/longears/pixelslinger
./pixelslinger --layout layouts/metal_tower_final.json --source basic-midi --dest spi &

echo 0 > /sys/class/leds/beaglebone\:green\:usr1/brightness
echo 0 > /sys/class/leds/beaglebone\:green\:usr2/brightness
echo 0 > /sys/class/leds/beaglebone\:green\:usr3/brightness

echo ========================================== DONE
echo ==========================================

