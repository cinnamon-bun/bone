
#================================================================================
# GIT SETUP

# tell git to ignore certificates (because there aren't any on the device)
git config --global http.sslVerify false

git config --global user.name "David Wallace"
git config --global user.email "david.wallace@gmail.com"

git clone https://davidwallace@bitbucket.org/davidwallace/bone.git
ln -s /home/root/bone/.profile .profile

# enable spi
cd bone/enable-spi
cp spi0-00A0.dtbo /lib/firmware
echo spi0 > /sys/devices/bone_capemgr.*/slots

# find address of spi
ll /dev | grep spi

#================================================================================
# SET UP USB CONNECTION

# set usb connection as gateway to the internet
route add default gw 192.168.7.1

# add DNS nameserver
echo "nameserver 8.8.8.8" >> /etc/resolv.conf



#================================================================================
# GET OPENPIXELCONTROL FILES

git clone https://github.com/longears/openpixelcontrol.git
cd openpixelcontrol
git fetch origin metal_tower_2
git checkout metal_tower_2
# edit makefile to remove extra targets
# edit tcl_server.c to point to correct spi address
make clean
make

# https://github.com/longears/pixelslinger

# update packages
opkg update
opkg install openssh-keygen
opkg install python-profile


#================================================================================
# install golang
# cd ~
# mkdir golang-install
# cd golang-install
# wget http://dave.cheney.net/paste/go1.1.1.linux-arm~armv7-1.tar.gz
# mkdir /usr/local
# tar -C /usr/local -xzf go1.1.1.linux-arm~armv7-1.tar.gz

opkg install tzdata-africa
opkg install tzdata-americas
opkg install tzdata-antarctica
opkg install tzdata-arctic
opkg install tzdata-asia
opkg install tzdata-atlantic
opkg install tzdata-australia
opkg install tzdata-europe
opkg install tzdata-misc
opkg install tzdata-pacific
opkg install tzdata-posix

cd ~
mkdir golang-install-from-source
cd golang-install-from-source
wget http://go.googlecode.com/files/go1.1.1.src.tar.gz 
tar zxvf go1.1.1.src.tar.gz
mv go /usr/local/go
cd /usr/local/go/src
./all.bash
# wait 45 minutes

# add my go repo
cd ~
mkdir gocode
cd ~/gocode
mkdir -p src/bitbucket.org/davidwallace
cd src/bitbucket.org/davidwallace
git clone https://davidwallace@bitbucket.org/davidwallace/go-opc.git




# Run a Script on Startup (cron)
# nano ~/startup.sh
# #put something in /tmp/ so we can see if this ran
# touch /tmp/WINNING
# #turn off the heartbeat
# echo 0 > /sys/class/leds/beaglebone\:green\:usr0/brightness
# chmod +x ~/startup.sh
# env EDITOR=nano crontab -e
# @reboot /bin/bash /home/root/startup.sh


#================================================================================
# disable services
systemctl list-units
systemctl disable cloud9.service

